backendApp.controller('FirstApprovalCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination,growl) {	
	$scope.filter = {};
	$scope.filter.amountFrom;
	$scope.filter.amountTo;
	$scope.filter.from;
	$scope.filter.to;
	$scope.filter.transactionType;
	$scope.dateManager = new Utils.DateManager();
	$scope.transactionsArr = [];
	$scope.filtered = {};
	$scope.filtered.transactionsArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.transactionsArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.transactionsArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$scope.initFiltersOnReady = function () {	
	  $scope.filter.transactionType = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
	}
	var initDependencies = {name: 'initList', dependencies: {getFirstApprovalInit: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getFirstApprovalInit('initList');});
    $scope.getFirstApprovalInit = function (dependencyName) {
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var transactionTypes = "transaction_payment_types";
		var methodRequest = {};
		methodRequest.data = 0;
		methodRequest.data = [];
	    methodRequest.data.push (
	    		transactionTypes
	    )
		$http.post('filters/get', methodRequest).then(
			function (response) {
				$scope.hideLoading();
				$rootScope.markDependencyDone($scope, dependencyName, 'getFirstApprovalInit');
				if (!$scope.handleErrors(response, 'getFirstApprovalInit')) {
					$scope.init = Utils.parseResponse(response);
					$scope.filter.transactionType.fillOptions($scope.parseFilter(Utils.parseResponse(response).transaction_payment_types));
				}
			}, function (response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});		
	}
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}
	$scope.listTransactionsFilter = function (transaction) {
		var hide = false;
//		// transaction from date filter
//		if ($scope.filter.from != null) {
//			if( $scope.filter.from.getTime() > transaction.createdFilter) {
//				hide = true;
//			}
//		}
//		// transaction to date filter
//		if ($scope.filter.to != null) {
//			if( $scope.filter.to.getTime() < transaction.createdFilter) {
//				hide = true;
//			}
//		}
		// transaction amount from filter
		if ($scope.filter.amountFrom != null) {
			if( parseInt($scope.filter.amountFrom) > transaction.amount) {
				hide = true;
			}
		}
		// transaction amount to filter
		if ($scope.filter.amountTo != null) {
			if( parseInt($scope.filter.amountTo) < transaction.amount) {
				hide = true;
			}
		}
		// transaction type filter
		if ($scope.filter.transactionType.getId() > -1) {
				if($scope.filter.transactionType.getId() != transaction.typeId) {
					hide = true;
				}
		}
		return !hide;
	}
    $scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data =  {
	    		transactionStatusId: 10,
	    		from: $scope.filter.from ? $scope.filter.from.getTime() : null,
	    		to: $scope.filter.to ? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
	    		};
        $http.post('transaction/getByStatus', methodRequest).then(
		function (response) {
				$scope.isLoading = false;
				$scope.getByStatusId = response;
				var transactions = response.data.data;
				var transactionsArr = [];
				for (transaction in transactions) {
					if (transactions.hasOwnProperty(transaction)) {
						transactionsArr.push({
						id: transactions[transaction].id,
						userId: transactions[transaction].user.id,
						amount: $rootScope.formatAmount({amount: transactions[transaction].amount, amountOnly: true}),
						paymentType: transactions[transaction].paymentType.displayName,
						created: transactions[transaction].timeCreated,
						typeId:transactions[transaction].paymentType.id,
					
                     })
                   }
				}
				$scope.transactionsArr = transactionsArr;
				$scope.updatePages();
			}, function (response) {
				// failure callback
				hideLoading();
				$scope.handleNetworkError(response);
			});
		
	}	
    $scope.updateStatus = function(transactionId){
    	$scope.transactionId=transactionId;
    	showLoading();
	    var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data =  {
	    		id: transactionId
	    		};
        $http.post('transaction/withdrawal/firstApproved', methodRequest).then(
		function (response) {
				hideLoading();
				$scope.getByStatusId = response;
				var transactions = response.data.data;
				$scope.search();
		    	if(response.data.responseCode==0)
		    		growl.success("transaction "+ $scope.transactionId +" first approved");
			}, function (response) {
				// failure callback
				hideLoading();
				$scope.handleNetworkError(response);
			});
    }
    $scope.cancel = function(transactionId,userId){
    	$scope.transactionId = transactionId;
    	$scope.userId = userId;
	  var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/cancelTransaction-popup.html',
			scope: $scope,
			controller: 'CancelTransactionPopupCtr',
			windowClass: 'cancelTransaction-popup'
		});
    }
	
	$scope.loadUser = function(userId){
		Utils.loadUserAndRedirect(userId);
	}
}]);
backendApp.controller('CancelTransactionPopupCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl',function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {	
	$scope.cancelTransaction = function() {
    var methodRequest = {};
    methodRequest.data = {
	   id: $scope.transactionId,
		user: {
			id: $scope.userId
		},
	};
    $http.post('transaction/withdrawal/cancel', methodRequest).then(
			function(response) {
				$scope.search();
				if(response.data.responseCode==0)
					growl.success("transaction "+ $scope.transactionId +" canceled")
	        },
	        function(response) {		
			}
        )
	}
}]);
backendApp.controller('SecondApprovalCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination,growl) {	
	$scope.filter = {};
	$scope.filter.amountFrom;
	$scope.filter.amountTo;
	$scope.filter.from;
	$scope.filter.to;
	$scope.filter.transactionType;
	$scope.dateManager = new Utils.DateManager();
	$scope.transactionsArr = [];
	$scope.filtered = {};
	$scope.filtered.transactionsArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.transactionsArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.transactionsArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$scope.initFiltersOnReady = function () {	
	  $scope.filter.transactionType = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
	}
	var initDependencies = {name: 'initList', dependencies: {getSecondApprovalInit: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getSecondApprovalInit('initList');});
    $scope.getSecondApprovalInit = function (dependencyName) {
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var transactionTypes = "transaction_payment_types";
		var methodRequest = {};
		methodRequest.data = 0;
		methodRequest.data = [];
	    methodRequest.data.push (
	    		transactionTypes
	    )
		$http.post('filters/get', methodRequest).then(
			function (response) {
				$scope.hideLoading();
				$rootScope.markDependencyDone($scope, dependencyName, 'getSecondApprovalInit');
				if (!$scope.handleErrors(response, 'getSecondApprovalInit')) {
					$scope.init = Utils.parseResponse(response);
					$scope.filter.transactionType.fillOptions($scope.parseFilter(Utils.parseResponse(response).transaction_payment_types));
				}
			}, function (response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});		
	}
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}
	$scope.listTransactionsFilter = function (transaction) {
		var hide = false;
//		// transaction from date filter
//		if ($scope.filter.from != null) {
//			if( $scope.filter.from.getTime() > transaction.createdFilter) {
//				hide = true;
//			}
//		}
//		// transaction to date filter
//		if ($scope.filter.to != null) {
//			if( $scope.filter.to.getTime() < transaction.createdFilter) {
//				hide = true;
//			}
//		}
		// transaction amount from filter
		if ($scope.filter.amountFrom != null) {
			if( parseInt($scope.filter.amountFrom) > transaction.amount) {
				hide = true;
			}
		}
		// transaction amount to filter
		if ($scope.filter.amountTo != null) {
			if( parseInt($scope.filter.amountTo) < transaction.amount) {
				hide = true;
			}
		}
		// transaction type filter
		if ($scope.filter.transactionType.getId() > -1) {
				if($scope.filter.transactionType.getId() != transaction.typeId) {
					hide = true;
				}
		}
	    return !hide;
	}
    $scope.search = function() {
    	$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data =  {
	    		transactionStatusId: 11,
	    		from: $scope.filter.from ? $scope.filter.from.getTime() : null,
	    		to: $scope.filter.to ? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
	    		};
	    $http.post('transaction/getByStatus', methodRequest).then(
        function (response) {
        		$scope.isLoading = false;
				$scope.getByStatusId = response;
				var transactions = response.data.data;
				var transactionsArr = [];
				for (transaction in transactions) {
					if (transactions.hasOwnProperty(transaction)) {
						var created = transactions[transaction].timeCreated;
						created = new Date(created);
						created = created.getTime();
						var createdFilter = transactions[transaction].timeCreated;
						createdFilter = new Date(createdFilter);
						createdFilter.setHours(0,0,0,0);
						createdFilter = createdFilter.getTime();
						transactionsArr.push({
						id: transactions[transaction].id,
						userId: transactions[transaction].user.id,
						amount: $rootScope.formatAmount({amount: transactions[transaction].amount, amountOnly: true}),
						paymentType: transactions[transaction].paymentType.displayName,
						created: created,
						createdFilter: createdFilter,
						typeId:transactions[transaction].paymentType.id,
                     })
                   }
				}
				$scope.transactionsArr = transactionsArr;
				$scope.updatePages();
			}, function (response) {
				// failure callback
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}
    $scope.updateStatus = function(transactionId){
    	showLoading();
	    var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data =  {
	    		id: transactionId
	    		};
        $http.post('transaction/withdrawal/secondApproved', methodRequest).then(
		function (response) {
			$scope.transactionId=transactionId;
				hideLoading();
				$scope.getByStatusId = response;
				var transactions = response.data.data;
				$scope.search();
		    	if(response.data.responseCode==0)
		    		growl.success("transaction "+ $scope.transactionId +" second approved");
			}, function (response) {
				// failure callback
				hideLoading();
				$scope.handleNetworkError(response);
			});
    }
    $scope.cancel = function(transactionId,userId){
    	$scope.transactionId = transactionId;
    	$scope.userId = userId;
	  var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/cancelTransaction-popup.html',
			scope: $scope,
			controller: 'CancelTransactionPopupCtr',
			windowClass: 'cancelTransaction-popup'
		});
    }
	
	$scope.loadUser = function(userId){
		Utils.loadUserAndRedirect(userId);
	}
}]);