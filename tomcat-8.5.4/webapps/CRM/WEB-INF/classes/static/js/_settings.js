var settings = {
	backendJsonLink: 'anycapital',
	urlPrefix: 'anycapital',
	domain: window.location.host,
	msgsPath: 'msgs/',
	msgsFileName: 'MessageResources_',
	msgsExtension: '.json',
	protocol: window.location.href.split('/')[0],
	skinId: 2,
	skinIdTexts: 2,
	skinLanguage: 'en',
	documentSizeLimit: 15,//MB
	loggedIn: false,
	serverTime: 0,
	serverOffsetMillis: 0,
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	defaultLanguage: 2,
	loginDetected: false,
	url_params: window.location.search,
	sessionTimeout: (1000*60*31)//31 min
}

settings.languages = [
	{
		languageId: 2,
		languageCode: 'en'
	},{
		languageId: 1,
		languageCode: 'de'
	}
];

settings.currencies = {
	2: {
		decimalPointDigits: 2,
		currencySymbol: 'currency.usd',
		currencyLeftSymbol: true,
		currencyName: 'currencies.usd'
	},
	3: {
		decimalPointDigits: 2,
		currencySymbol: 'currency.eur',
		currencyLeftSymbol: true,
		currencyName: 'currencies.eur'
	}
};

settings.actionSources = {
	WEB: {
		id: 1,
		name: 'WEB'
	},
	CRM: {
		id: 2,
		name: 'CRM'
	},
	JOB: {
		id: 3,
		name: 'JOB'
	}
}

settings.paymentTypes = {
		BANK_WIRE: {
			id:1,
			name:'BANK_WIRE'
		},ADMIN: {
			id:3,
			name:'ADMIN'
		}
}

var PAGE_LOGIN_ACCESS_TYPES = {
	any: 0,
	loggedOut: 1,
	loggedIn: 2
}

var errorCodeMap = {
	success: 0,
	access_denied: 601,
	authentication_failure: 602,
	invalid_input: 701,
	unknown: 999,
	invalid_password: 705,
	cancel_inv_negetive_balance: 750,
}

TOAST_TYPES = {
	info: 1,
	error: 2
}

var amountDivideBy100 = true;

//regExp
var regEx_lettersAndNumbers = /^[0-9a-zA-Z]+$/;
var regEx_nickname = /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/;
var regEx_nickname_reverse = /[^a-zA-Z0-9!@#$%\^:;"']/g;
var regEx_phone  = /^\d{7,20}$/;
var regEx_digits = /^\d+$/;
var regEx_digits_reverse  = /\D/g;
var regEx_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regEx_englishLettersOnly = /^['a-zA-Z ]+$/;
var regEx_lettersOnly = "^['\\p{L} ]+$";