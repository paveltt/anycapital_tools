backendApp.controller('InvestmentsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Pagination','growl','SingleSelect', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Pagination,growl,SingleSelect) {	
	$scope.investmentsArr = [];
	$scope.filtered = {};
	$scope.filtered.investmentsArr = [];
	$scope.dateManager = new Utils.DateManager();
	$scope.filter = {};
	$scope.filter.from;
	$scope.filter.to;
	$scope.filter.userClass;
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.searchDisabled = true;
	$scope.isSearchDisabled  = function () {
		return $scope.searchDisabled; 
	}	
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.investmentsArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.investmentsArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$scope.filter.userClass = new SingleSelect.InstanceClass({aggregateOption: {id: null, name: 'All'}});
	var user_class = "user_class"; 
	var methodRequest = {};
    methodRequest.data = [];
    methodRequest.data.push (
    		user_class
    )
	$http.post('filters/get', methodRequest).then(
		function (response) {
				$scope.init = Utils.parseResponse(response);
				$scope.filter.userClass.fillOptions(Utils.parseResponse(response).user_class);
				$scope.filter.userClass.setModel($scope.filter.userClass.getOptionById(2));
				$scope.searchDisabled = false;
		}, function (response) {
			$scope.hideLoading();
			$scope.handleNetworkError(response);
		});		
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}	
    $scope.search = function() {
    	$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = {
	    		from : $scope.filter.from != null ? $scope.filter.from.getTime() : null,
	    		to : $scope.filter.to != null ? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
	    		userClassId: $scope.filter.userClass.getId()
	    };
        $http.post('investment/get', methodRequest).then(
		function (response) {
				$scope.isLoading = false;
				var investments = response.data.data;
				$scope.investmentsArr = investments.map(function(investment) {
					return {
						id: investment.id,
						productId: investment.productId,
						amount: $rootScope.formatAmount({amount: investment.amount, amountOnly: true}),
						ask: investment.ask,
						bid: investment.bid,
						userId: investment.user.id,
						type: investment.investmentType.id,
						typeName: investment.investmentType.displayName,
						status: investment.investmentStatus.id,
						statusName: investment.investmentStatus.displayName,
						created: investment.timeCreated,							   
					 }
				});
				$scope.pagination.setPages($scope.page, $scope.investmentsArr.length, $scope.resultsPerPage);
				
			}, function (response) {
				// failure callback
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}
    $scope.sumAmount = function() {
        var total = 0;
        for(i = 0; i < $scope.investmentsArr.length; i++) {
            total += $scope.investmentsArr[i].amount;
        }
        return total.toFixed(2);
    };
    var cancelDisabledList = [];
    
    $scope.isCancelDisabled = function(investmentId) {
    	var index = cancelDisabledList.indexOf(investmentId)
    	return index === -1 ? false : true;
    }
    
    function disableCancelButton(investmentId) {
    	cancelDisabledList.push(investmentId)
    }
    
    function enableCancelButton(investmentId) {
    	var index = cancelDisabledList.indexOf(investmentId)
    	if (index !== -1) {
        	cancelDisabledList.splice(index, 1);
    	}
    }
    
    $scope.cancel = function(investmentId) {
    	disableCancelButton(investmentId);
    	$scope.showLoading();
    	var methodRequest = {};
    	methodRequest.data = {id: investmentId};
    	$http.post('investment/cancel',methodRequest).then(
    			function(response){
    				$scope.hideLoading();
    				$scope.search();
    				if (response.data.responseCode == errorCodeMap.success) {
    					growl.success('Investment ' + investmentId + ' canceled');
    				} else if (response.data.responseCode == errorCodeMap.invalid_input) {
    					growl.error('Investment id - ' + investmentId + ' is invalid');
    					enableCancelButton(investmentId);
    				} else if (response.data.responseCode == errorCodeMap.cancel_inv_negetive_balance) {
    					growl.error('Can\'t cancel investment '+ investmentId +' - user will get into negative balance ');
    					enableCancelButton(investmentId);
    				}
    			},function(response){
    				enableCancelButton(investmentId);
    			}
    			);
    };
    
    
    $scope.sumAmountPerPage = function() {
    	return $scope.filtered.investmentsArr.reduce(function(sum, investment) {
    		return sum + investment.amount;
    	}, 0).toFixed(2);
    };
	
	$scope.loadUser = function(userId){
		Utils.loadUserAndRedirect(userId);
	}
      
}]);