if(typeof window.folderPrefix == 'undefined'){
	var folderPrefix = './html/templates/';
}
if(typeof window.indexTemplate == 'undefined'){
	var indexTemplate = 'index.html';
}

var backendApp = angular.module('backendApp', [
	'ui.router',
	'angularFileUpload',
	'ngSanitize',
	'ngAnimate',
	'vcRecaptcha',
	'ui.bootstrap',
	'ui.bootstrap.datetimepicker', 
	'ui.tinymce', 
	'ui.tree',
	'angular-growl',
	'chart.js'
])
.run(['$rootScope', '$state', '$stateParams', '$interval', '$timeout', '$location',  '$window', 'lastRequestError',
	function ($rootScope, $state, $stateParams, $interval, $timeout, $location, $window, lastRequestError) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.For example,
		// <li ng-class='{ active: $state.includes('contacts.list') }'> will set the <li>
		// to active whenever 'contacts.list' or one of its decendents is active.
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.PAGE_LOGIN_ACCESS_TYPES = PAGE_LOGIN_ACCESS_TYPES;
		
		$rootScope.ready = false;
		
		$rootScope.languageDetected = false;
		
		$rootScope.folderPrefix = folderPrefix;
		
		$rootScope.languages = settings.languages;
		$rootScope.language = settings.languages[0];
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			
			if($rootScope.$state.params.ln){
				var languageKey = searchJsonKeyInArray(settings.languages, 'languageCode', $rootScope.$state.params.ln);
				if(languageKey != -1){
					$rootScope.language = settings.languages[languageKey];
				}
				if(!$rootScope.languageDetected){
					$rootScope.languageDetected = true;
				}
			}
			
			if($rootScope.ready){
				checkState();
			}else{
				var checkStateWatch = $rootScope.$watch(function(){return $rootScope.ready;}, function(){
					if($rootScope.ready){
						checkStateWatch();
						checkState();
					}
				});
			}
			function checkState(){
				if (toState.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedIn && !$rootScope.isLogged) {
					$rootScope.$state.go('ln.login', {ln: $rootScope.$state.params.ln});
					return;
				}
				if(toState.loginAccessType == PAGE_LOGIN_ACCESS_TYPES.loggedOut && $rootScope.isLogged){
					$rootScope.$state.go('ln.home', {ln: $rootScope.$state.params.ln});
					return;
				}
			}
			
			window.scrollTo(0, 0);
			//redirect links that should not be accessed
			if (!isUndefined($rootScope.$state.current.redirectTo)) {
				$rootScope.$state.go($rootScope.$state.current.redirectTo, {ln: $rootScope.$state.params.ln}, {location: 'replace'});
			}
			
			if($rootScope.menu){
				$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
			}
			$rootScope.$broadcast('resetGlobalErrorMsg');
			lastRequestError.setError('');
		});
		
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams) {
				//change browser title
				if (!isUndefined(toState.metadataKey)) {
					$rootScope.metadataTitle = toState.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
				
				//close any open ppups
				//$rootScope.closeCopyOpPopup({all:true});
			})
			
		$rootScope.$on('unauthorized', function() {
			$rootScope.isLogged = false;
			$rootScope.setLoggedUser();
			$state.go('ln.login');
		}); 
			
	}]
)
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider','$locationProvider', 'lastRequestErrorProvider','growlProvider',
    function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider, lastRequestErrorProvider,growlProvider) {
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		//delete $httpProvider.defaults.headers.post['Content-Type'];
		$httpProvider.defaults.withCredentials = true;
		//growl alerts configuration
		growlProvider.globalTimeToLive(8000);
		//growlProvider.globalDisableCountDown(true);
		growlProvider.globalPosition("top-center");
		$httpProvider.interceptors.push(['$rootScope',function($rootScope) {
			return {
				request: function(config) {  
					if (config.method === 'POST' && Object(config.data) === config.data) {
						config.data.actionSource = settings.actionSources.CRM.id;
					}
					return config;
				},
				response: function(response) {
					if (response.config.method === 'POST' && Object(response.data) === response.data) {
						var data = response.data;
						var errMsg = '';
					   if (response.data.error == null){
						   if (response.data.responseCode == errorCodeMap.access_denied){ 
							   errMsg = 'access denied';
							   $rootScope.$emit("unauthorized");
							   console.log('emit');
						   } else if (response.data.responseCode == errorCodeMap.invalid_input){
							   errMsg = 'invalid input';
						   } else if (response.data.responseCode == errorCodeMap.unknown){
							   errMsg = 'please try again';
						   } else if (response.data.responseCode == errorCodeMap.invalid_password){
							   errMsg = 'invalid password';
						   }
					   } else {
						   if (response.data.error.messages != null) {
							   errMsg = response.data.error.messages[0].field.replace('data','').replace('.','') + " " + response.data.error.messages[0].content;
						   } else {
							   errMsg = response.data.error.reason;
						   }
					   }
					   lastRequestErrorProvider.setError(errMsg);
					}
					return response;
				} 
			};
		}]);
		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
		$urlRouterProvider
		// The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
		// If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
		.when('/', '/en/')
		.otherwise('/en/');

		// Use $stateProvider to configure your states.
		$stateProvider
			.state('ln', {
				url: '/:ln',
				templateUrl: folderPrefix + 'template.html',
				'abstract': true
			})
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: folderPrefix + indexTemplate
					}
				},
				redirectTo: 'ln.home'
			})
			.state('ln.login', {
				url: '/login',
				views: {
					'main': {
						templateUrl: folderPrefix + 'login.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedOut
			})
			.state('ln.home', {
				url: '/home',
				views: {
					'main': {
						templateUrl: folderPrefix + 'home.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			.state('analytics', {		
				url: '/analytics',
				templateUrl: folderPrefix + 'analytics.html',
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			//Administrator space
			.state('ln.admin', {
				url: '/admin',
				views: {
					'main': {
						templateUrl: folderPrefix + 'admin/admin-template.html'
					}
				},
				'abstract': true
			})
			.state('ln.admin.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/admin-index.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			.state('ln.admin.translations', {
				url: '/translations',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/translations.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			.state('ln.admin.translations.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/translations.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			.state('ln.admin.bulkTranslations', {
				url: '/bulkTranslations',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/bulkTranslations.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})			
			.state('ln.admin.products', {
				url: '/products',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/products.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.productsPriority', {
				url: '/productsPriority',
				views: {
					'main-content': {
						template: '<products-priority />'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			
			})
			
			.state('ln.admin.wireWithdraw', {
				url: '/wireWithdraw',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/wireWithdraw.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.adminWithdraw', {
				url: '/adminWithdraw',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/adminWithdraw.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.adminDeposit', {
				url: '/adminDeposit',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/adminDeposit.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.allProducts', {
				url: '/allProducts',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/allProducts.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.writer-change-pass', {
				url: '/writer-change-pass',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writerChangePass.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.admin-panel', {
				url: '/admin-panel',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/adminPanel.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})			
			
			.state('ln.admin.writers', {
				url: '/writers',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writers.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.writers.edit', {
				url: '/edit-:id',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writers.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.writers.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writers.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.dbParameters', {
				url: '/dbParameters',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/dbParameters.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.dbParameters.edit', {
				url: '/edit-:id',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/dbParameters.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.dbParameters.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/dbParameters.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			
			
			.state('ln.admin.firstApproval', {
				url: '/firstApproval',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/firstApproval.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			.state('ln.admin.secondApproval', {
				url: '/secondApproval',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/secondApproval.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.investments', {
				url: '/investments',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/investments.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			.state('ln.admin.userInvestments', {
				url: '/userInvestments',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/userInvestments.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.addUser', {
				url: '/addUser',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/addUser.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.editUser', {
				url: '/editUser',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/editUser.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.wireDeposit', {
				url: '/wireDeposit',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/wireDeposit.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.changePassword', {
				url: '/changePassword',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/changePassword.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.loadUser', {
				url: '/loadUser',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/loadUser.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.addUrl', {
				url: '/addUrl',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/addUrl.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})

			.state('ln.admin.transactions', {
				url: '/transactions',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/transactions.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.allTransactions', {
				url: '/allTransactions',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/allTransactions.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
			
			.state('ln.admin.marketingUrls', {
				url: '/marketingUrls',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingUrls.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn

			})
						
			.state('ln.admin.products.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/products.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			.state('ln.admin.products.edit', {
				url: '/edit-:productId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/products.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.sources', {
				url: '/sources',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingSources.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.sources.edit', {
				url: '/edit-:sourceId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingSources.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.sources.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingSources.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.landingPages', {
				url: '/landingPages',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingLandingPages.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.landingPages.edit', {
				url: '/edit-:landingPageId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingLandingPages.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.landingPages.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingLandingPages.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.contents', {
				url: '/contents',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingContents.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.contents.edit', {
				url: '/edit-:contentId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingContents.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.contents.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingContents.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.campaigns', {
				url: '/campaigns',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingCampaigns.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.campaigns.edit', {
				url: '/edit-:campaignId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingCampaigns.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.campaigns.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/marketingCampaigns.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			.state('ln.admin.uploadContacts', {
				url: '/uploadContacts',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/uploadContacts.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})			
			.state('ln.admin.issues', {
				url: '/issues',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/issues.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.issues.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/issues.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.files', {
				url: '/files',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/files.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.files.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/files.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.files.edit', {
				url: '/edit-:fileId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/files.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.regulation', {
				url: '/regulation',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/regulation.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.templates', {
				url: '/templates',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/templates.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.clientSpace', {
				url: '/clientSpace',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/clientSpace.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			.state('ln.admin.advancedSearchUser', {
				url: '/advancedSearchUser',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/userAdvancedSearch.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})			
			
			.state('ln.admin.userAnswers', {
				url: '/userAnswers',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/userAnswers.html'
					}
				},
				loginAccessType: PAGE_LOGIN_ACCESS_TYPES.loggedIn
			})
			
			// enable html5Mode for pushstate ('#'-less URLs) //requires base tag in the head
			/*if (settings.isLive) {
				$locationProvider.html5Mode(true).hashPrefix('!');
			}*/
	}]);
	
backendApp.config(['$compileProvider', function ($compileProvider) {
	// $compileProvider.debugInfoEnabled(false);
}]);



backendApp.service('Utils', ['$state', 'userService', function($state, userService){
	var parseResponse = function(response){
		if(!response || !response.data){
			return null;
		}
		return response.data.data;
	}
	
	var DateManager = function(params){
		if(!params){
			params = {};
		}
		
		this.openDates = {};
		if(params.openDates && Array.isArray(params.openDates)){
			for(var i = 0; i < params.openDates.length; i++){
				this.openDates[params.openDates[i]] = false;
			}
		}
		this.format = params.format;
		this.onOpenCallback = params.onOpenCallback;
		
		if(!this.format){
			this.format = 'dd/MM/yyyy';
		}
		
		this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00',
			ngModelOptions: {
				timezone: 'UTC'
			}
		};
		
		this.openCalendar = function(e, date){
			if(this.onOpenCallback){
				this.onOpenCallback();
			}
			this.openDates[date] = true;
		}
		
		this.save = function(date){
			return date;
		}
	}
	
	var loadingCounter = [];
	function showLoading(globalOverlay){
		loadingCounter.push(globalOverlay);
		$('.content').addClass('loading');
		if(globalOverlay){
			$('.content').addClass('loading-global');
		}
	}
	function hideLoading(){
		loadingCounter.pop();
		if(loadingCounter.length == 0){
			$('.content').removeClass('loading');
		}
		var hasGlobal = false;
		for(var i = 0; i < loadingCounter.length; i++){
			if(loadingCounter[i]){
				hasGlobal = true;
			}
		}
		if(!hasGlobal){
			$('.content').removeClass('loading-global');
		}
	}
	
	function loadUserAndRedirect(userId, userEmail){
		userService.loadUser(userId, null);
		$state.go('ln.admin.loadUser');
	}
	
	return {
		settings: settings,
		parseResponse: parseResponse,
		DateManager: DateManager,
		showLoading: showLoading,
		hideLoading: hideLoading,
		loadUserAndRedirect: loadUserAndRedirect
	};
}]);


//Class definitions
backendApp.service('Permissions', [function(){
	var Permissions = function(permissions){
		var rawPermissions = cloneObj(permissions);
		
		var parsePermissions = function(tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					parsePermissions(tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					parsePermissions(tree[i], list);
				}
			}else{
				list.push({id: tree.id, allowed: false});
			}
			
			return list;
		}
		
		this.permissionsList = parsePermissions(rawPermissions);
		rawPermissions = null;
		
		this.set = function(permissions){
			this.permissionsList = [];
			for(space in permissions){
				if(permissions.hasOwnProperty(space)){
					for(screen in permissions[space].beScreeMap){
						if(permissions[space].beScreeMap.hasOwnProperty(screen)){
							for(permissionKey in permissions[space].beScreeMap[screen].bePermissionList){
								if(permissions[space].beScreeMap[screen].bePermissionList.hasOwnProperty(permissionKey)){
									var permissionId = space + '_' + screen + '_' + permissions[space].beScreeMap[screen].bePermissionList[permissionKey].name;
									this.permissionsList.push({id: permissionId, allowed: true});
								}
							}
						}
					}
				}
			}
		}
		
		this.get = function(permission){
			//Allow wildcard
			if(permission == '*'){
				return true;
			}
			for(var i = 0; i < this.permissionsList.length; i++){
				if(this.permissionsList[i].id == permission){
					return this.permissionsList[i].allowed;
				}
			}
			return false;
		}
		
		this.allow = function(permissionId){
			var found = false;
			for(var i = 0; i < this.permissionsList.length; i++){
				if(this.permissionsList[i].id == permissionId){
					this.permissionsList[i].allowed = true;
					found = true;
				}
			}
			if(!found){
				this.permissionsList.push({id: permissionId, allowed: true});
			}
		}
	}
	return {
		InstanceClass: Permissions
	}
}]);


backendApp.service('BaseTree', [function(){
	var BaseTree = function(tree, active){
		var that = this;
		
		this.root = cloneObj(tree);
		this.active = (typeof active == "undefined") ? false : active;
		
		function initTree(tree, parent){
			if(typeof parent == "undefined"){
				parent = null;
			}
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					tree[i].parent = parent;
					tree[i].root = that.root;
					if(!tree[i].active){
						tree[i].active = that.active;
					}
					if(!tree[i].collapsed){
						tree[i].collapsed = false;
					}
					if(tree[i].nodes){
						initTree(tree[i].nodes, tree[i]);
					}
				}
			}
		}
		
		initTree(this.root);
		
		this.getTreeTop = function(node){
			if(node.nodes && node.parent){
				return getTreeTop(node.parent);
			}else if(node.parent == null){
				return node;
			}else{
				return null;
			}
		}
	}
	
	BaseTree.toggleNode = function(node){
		node.collapsed = !node.collapsed;
	}
	
	return {
		InstanceClass: BaseTree
	}
}]);

backendApp.service('MenuTree', ['BaseTree', function(BaseTree){
	var MenuTree = function(tree, active, permissions){
		//Inherit BaseTree
		BaseTree.InstanceClass.apply(this, arguments);
		
		var that = this;
		
		this.uncollapseCurrent = function(tree){
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					if(tree[i].current){
						var node = tree[i].parent;
						while(node != null){
							if(node.hasOwnProperty('collapsed')){
								node.current = true;
								node.collapsed = false;
							}
							node = node.parent;
						}
						parent = null;
					}
					if(tree[i].nodes){
						this.uncollapseCurrent(tree[i].nodes);
					}
				}
			}
		}
		
		this.uncollapseCurrent(this.root);
		
		this.updateMenuNodes = function(validateCallback){
			updateMenuNodesRecursive(this.root, validateCallback);
			this.uncollapseCurrent(this.root);
		}
		
		function updateMenuNodesRecursive(tree, validateCallback){
			for(var i = 0; i < tree.length; i++){
				if(validateCallback(tree[i].link)){
					tree[i].current = true;
				}else{
					tree[i].current = false;
				}
				if(tree[i].nodes){
					tree[i].collapsed = true;
					updateMenuNodesRecursive(tree[i].nodes, validateCallback);
				}else{
					if(!permissions.get(tree[i].permission)){
						tree.splice(i, 1);
						i--;
					}
				}
			}
		}
	}
	
	//Inherit static members
	for(staticMember in BaseTree.InstanceClass){
		if(BaseTree.InstanceClass.hasOwnProperty(staticMember)){
			MenuTree[staticMember] = BaseTree.InstanceClass[staticMember];
		}
	}
	
	return {
		InstanceClass: MenuTree
	}
}]);

backendApp.service('CheckboxTree', ['BaseTree', function(BaseTree){
	var CheckboxTree = function(tree, active){
		//Inherit BaseTree
		BaseTree.InstanceClass.apply(this, arguments);
		
		var that = this;
		
		function initTree(tree, parent){
			if(typeof parent == "undefined"){
				parent = null;
			}
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					if(!tree[i].nodes){
						if(!tree[i].checked){
							tree[i].checked = false;
						}
					}else{
						if(!tree[i].checkedStatus){
							tree[i].checkedStatus = 0;
						}
						initTree(tree[i].nodes, tree[i]);
					}
				}
			}
		}
		
		initTree(this.root);
		
		this.normalizeCheckedStatus = function(){
			this.normalizeCheckedStatusRecursive(this.root);
		}
		
		this.normalizeCheckedStatusRecursive = function(tree){
			if(tree.nodes){
				var numLeaves = 0;
				var countUnchecked = 0;
				var countSemiChecked = 0;
				var countChecked = 0;
				for(var i = 0; i < tree.nodes.length; i++){
					if(!tree.nodes[i].nodes){
						numLeaves++;
						if(tree.nodes[i].checked){
							countChecked++;
						}else{
							countUnchecked++;
						}
					}else{
						var temp = this.normalizeCheckedStatusRecursive(tree.nodes[i]);
						if(temp == 2){
							countChecked++;
						}else if(temp == 1){
							countSemiChecked++;
						}else{
							countUnchecked++;
						}
					}
				}
				if(countSemiChecked == 0 && countChecked == 0){
					tree.checkedStatus = 0;
				}else if(countUnchecked == 0 && countSemiChecked == 0){
					tree.checkedStatus = 2;
				}else{
					tree.checkedStatus = 1;
				}
				return tree.checkedStatus;
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.normalizeCheckedStatusRecursive(tree[i]);
				}
			}else{
				return;
			}
		}
		
		this.checkNode = function(tree, value){
			if(tree.name && tree.name == value){
				tree.checked = true;
			}
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.checkNode(tree.nodes[i], value);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.checkNode(tree[i], value);
				}
			}
		}
		
		this.uncheckNode = function(tree, value){
			if(tree.name && tree.name == value){
				tree.checked = false;
			}
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.uncheckNode(tree.nodes[i], value);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.uncheckNode(tree[i], value);
				}
			}
		}
		
		this.getCheckedNodes = function(onlyChanged, tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			if(typeof tree == "undefined"){
				tree = this.root;
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.getCheckedNodes(onlyChanged, tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.getCheckedNodes(onlyChanged, tree[i], list);
				}
			}else{
				if(tree.checked && (!onlyChanged || (tree.changed && tree.checked != tree.originalChecked))){
					list.push(tree.id);
				}
			}
			
			return list;
		}
		
		this.getUncheckedNodes = function(onlyChanged, tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			if(typeof tree == "undefined"){
				tree = this.root;
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.getUncheckedNodes(onlyChanged, tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.getUncheckedNodes(onlyChanged, tree[i], list);
				}
			}else{
				if(!tree.checked && (!onlyChanged || (tree.changed && tree.checked != tree.originalChecked))){
					list.push(tree.id);
				}
			}
			
			return list;
		}
	}
	
	//Inherit static members
	for(staticMember in BaseTree.InstanceClass){
		if(BaseTree.InstanceClass.hasOwnProperty(staticMember)){
			CheckboxTree[staticMember] = BaseTree.InstanceClass[staticMember];
		}
	}
	
	CheckboxTree.updateTree = function(node){
		if(node.active){
			CheckboxTree.updateTreeCheckedDown(node);
			CheckboxTree.updateTreeCheckedUp(node);
		}
	}
	
	CheckboxTree.updateTreeCheckedDown = function(node){
		var checkedStatus = CheckboxTree.getCheckedStatus(node);
		if(checkedStatus == 0){
			CheckboxTree.updateNodeStatusDown(node, 2);
		}else if(checkedStatus == 1){
			CheckboxTree.updateNodeStatusDown(node, 2);
		}else{
			CheckboxTree.updateNodeStatusDown(node, 0);
		}
	}
	
	CheckboxTree.updateNodeStatusDown = function(node, status){
		if(node.nodes){
			node.checkedStatus = status;
			for(var i = 0; i < node.nodes.length; i++){
				if(node.nodes[i].nodes){
					node.nodes[i].checkedStatus = status;
					CheckboxTree.updateNodeStatusDown(node.nodes[i], status);
				}else{
					if(!node.nodes[i].changed){
						node.nodes[i].changed = true;
						node.nodes[i].originalChecked = node.nodes[i].checked;
					}
					node.nodes[i].checked = (status == 0) ? false : true;
				}
			}
		}else{
			if(!node.changed){
				node.changed = true;
				node.originalChecked = node.checked;
			}
			node.checked = (status == 0) ? false : true;
		}
	}
	
	CheckboxTree.updateTreeCheckedUp = function(node){
		var checkedStatus = CheckboxTree.getCheckedStatus(node);
		if(node.parent){
			if(checkedStatus == 0){
				CheckboxTree.updateNodeStatusUp(node.parent, 2);
			}else if(checkedStatus == 1){
				CheckboxTree.updateNodeStatusUp(node.parent, 2);
			}else{
				CheckboxTree.updateNodeStatusUp(node.parent, 0);
			}
		}
	}
	
	CheckboxTree.updateNodeStatusUp = function(node){
		if(node.nodes){
			var status = 0;
			var countUnchecked = 0;
			var countSemiChecked = 0;
			var countChecked = 0;
			for(var i = 0; i < node.nodes.length; i++){
				if(node.nodes[i].nodes){
					if(node.nodes[i].checkedStatus == 0){
						countUnchecked++;
					}else if(node.nodes[i].checkedStatus == 1){
						countSemiChecked++;
					}else if(node.nodes[i].checkedStatus == 2){
						countChecked++;
					}
				}else{
					if(node.nodes[i].checked){
						countChecked++;
					}else{
						countUnchecked++;
					}
				}
			}
			if(countSemiChecked == 0 && countChecked == 0){
				status = 0;
			}else if(countUnchecked == 0 && countSemiChecked == 0){
				status = 2;
			}else{
				status = 1;
			}
			node.checkedStatus = status;
			if(node.parent){
				CheckboxTree.updateNodeStatusUp(node.parent);
			}
		}
	}
	
	CheckboxTree.getCheckedStatus = function(node){
		var checkedStatus = 0;
		if(node.nodes){
			checkedStatus = node.checkedStatus;
		}else{
			checkedStatus = node.checked ? 2 : 0;
		}
		return checkedStatus;
	}
	
	return {
		InstanceClass: CheckboxTree
	}
}]);


backendApp.service('Pagination', [function(){
	var Pagination = function(params){
		var that = this;
		
		this.pages = [];
		this.currentPage = 1;
		this.maxPage = 1;
		this.totalCount = 0;
		this.resultsPerPage = 20;
		
		this.callback = params.callback;
		this.hideSummary = params.hideSummary;
		this.hideGroupNavigation = params.hideGroupNavigation;
		
		this.setPages = function(currentPage, totalCount, resultsPerPage){
			this.currentPage = currentPage;
			this.maxPage = Math.ceil(totalCount/resultsPerPage);
			this.totalCount = totalCount;
			this.resultsPerPage = resultsPerPage;
			this.pages = [];
			for(var i = Math.max(0, currentPage - 4); i < Math.min(this.maxPage, Math.max(0, currentPage - 4) + 10); i++){
				this.pages.push(i+1);
			}
		}
		
		this.validatePage = function(page){
			if(typeof page != 'undefined' && page == this.currentPage){
				return false;
			}
			if(typeof page == 'undefined'){
				page = 1;
			}
			return page;
		}
		
		this.setPages(this.currentPage, this.maxPage);
		
		this.previousPage = function(){
			return Math.max(this.currentPage - 1, 1);
		}
		this.nextPage = function(){
			return Math.min(this.currentPage + 1, this.maxPage);
		}
		this.previousPageGroup = function(){
			return Math.max(this.currentPage - 10, 1);
		}
		this.nextPageGroup = function(){
			return Math.min(this.currentPage + 10, this.maxPage);
		}
	}
	
	return {
		InstanceClass: Pagination
	}
}]);


backendApp.service('SingleSelect', ['$timeout', '$rootScope', function($timeout, $rootScope){
	var SingleSelect = function(params){
		var that = this;
		
		if(!params){
			params = {};
		}
		
		this.visible = false;
		
		this.externalModel = params.externalModel;
		if(this.externalModel){
			if(this.externalModel.bindById !== false){
				this.externalModel.bindById = true;
			}
			if(this.externalModel.bindByIdInt !== false){
				this.externalModel.bindByIdInt = true;
			}
		}
		this.validateExternalModel = function(){
			if(this.externalModel && typeof this.externalModel.objCallback == 'function' && (typeof this.externalModel.objCallback() != 'undefined' && this.externalModel.objCallback() != null)){
				return true;
			}
			return false;
		}
		$timeout(function(){
			$rootScope.$watch(function(){
				return that.validateExternalModel() ? that.externalModel.objCallback()[that.externalModel.key] : '';
			}, function(newValue, oldValue){
				if(!that.validateExternalModel()){
					return;
				}
				if(that.externalModel.bindById){
					if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model.id){
						return;
					}
					that.model = null;
					for(var i = 0; i < that.options.length; i++){
						if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i].id){
							that.model = (typeof that.model == 'string') ? that.options[i] + '' : that.options[i];
						}
					}
				}else{
					if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model){
						return;
					}
					that.model = null;
					for(var i = 0; i < that.options.length; i++){
						if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i]){
							that.model = that.options[i];
						}
					}
				}
			}, true);
		});
		
		this.isSmart = params.isSmart;
		this.noSort = params.noSort;
		this.sortById = params.sortById;
		this.sortByNameInt = params.sortByNameInt;
		this.mutateCallback = params.mutateCallback;
		this.mutateOnlyAggregate = params.mutateOnlyAggregate;
		this.iconCallback = params.iconCallback;
		
		this.filterText = '';
		this.filterCallback = params.filterCallback;
		
		this.filterPlaceholder = params.filterPlaceholder;
		
		this.model = null;
		this.options = [];
		this.aggregateOption = params.aggregateOption;
		
		this.setModel = function(option){
			this.model = option;
			if(this.externalModel && typeof this.externalModel.objCallback == 'function'){
				if(this.externalModel.bindById){
					this.externalModel.objCallback()[this.externalModel.key] = this.externalModel.bindByIdInt ? parseInt(this.model.id) : (this.model.id ? this.model.id : null);
				}else{
					this.externalModel.objCallback()[this.externalModel.key] = this.model;
				}
			}
		}
		
		if(this.aggregateOption){
			this.aggregateOption.isAggregate = true;
			this.options.push(this.aggregateOption);
			this.setModel(this.aggregateOption);
		}
		
		this.fillOptions = function(options, setModelToFirst){
			this.options = [];
			if(this.aggregateOption){
				this.options.push(this.aggregateOption);
			}
			if(options && Array.isArray(options)){
				for(var i = 0; i < options.length; i++){
					this.options.push(options[i]);
				}
			}else if(options){
				for(var el in options){
					if(options.hasOwnProperty(el)){
						var name = options[el];
						this.options.push({id: el, name: name});
					}
				}
			}
			if(setModelToFirst && this.options.length > 0){
				this.setModel(this.options[0]);
			}
		}
		
		if(params.options){
			this.fillOptions(params.options);
		}
		
		this.setFilterCallback = function(callback){
			this.filterCallback = callback;
		}
		
		this.filter = function(option){
			var filterTextMatch = true;
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				filterTextMatch = that.mutateCallback(option.name).search(new RegExp(that.filterText, "i")) != -1;
			}else{
				filterTextMatch = option.name.search(new RegExp(that.filterText, "i")) != -1;
			}
			if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
				var filterCallbackResult = that.filterCallback(option);
				if(!filterCallbackResult){
					if(that.model == option){
						//TODO - perhaps find the first option that would not be filtered by filterCallback and set the model to that option
						if(that.aggregateOption){
							that.model = that.aggregateOption;
						}else{
							that.model = null;
						}
					}
				}
				return filterTextMatch && filterCallbackResult;
			}
			return filterTextMatch;
		}
		
		this.sort = function(option){
			if(that.noSort){
				return '';
			}
			if(that.sortById){
				return option == that.aggregateOption ? -1000 : parseInt(option.id);
			}
			if(that.sortByNameInt){
				return option == that.aggregateOption ? -1000 : parseInt(option.name);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option == that.aggregateOption)){
				return option == that.aggregateOption ? '' : that.mutateCallback(option.name).trim();
			}
			return option == that.aggregateOption ? '' : option.name.trim();
		}
		
		this.getTextValue = function(){
			if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
				this.model != null ? this.mutateCallback(this.model.name) : '';
			}
			return this.model != null ? this.model.name : '';
		}
		
		this.getId = function(){
			if(!this.model){
				return null;
			}
			return this.model.id;
		}
		
		this.getOptionById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return null;
			}
			return val;
		}
		
		this.getElNameById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return '';
			}
			if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
				this.mutateCallback(val.name);
			}
			return val.name;
		}
		
		this.setById = function(id){
			this.setModel(this.getOptionById(id));
		}
		
		this.getPreviousOption = function(){
			if(this.model.index == 0){
				return null;
			}
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index == this.model.index - 1){
					return this.options[i];
				}
			}
			return null;
		}
		this.getNextOption = function(){
			if(this.model.index == this.options.length - 1){
				return null;
			}
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index == this.model.index + 1){
					return this.options[i];
				}
			}
			return null;
		}
		this.getNextOptionByKey = function(key){
			var firstMatch = null;
			var nextMatch = null;
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index != this.model.index){
					var name = this.options[i].name;
					if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
						name = this.mutateCallback(name);
					}
					if(name && name.substring(0, 1).localeCompare(key, undefined, {sensitivity: 'base'}) == 0){
						if(this.options[i].index < this.model.index && (firstMatch == null || firstMatch.index > this.options[i].index)){
							firstMatch = this.options[i];
						}
						if(this.options[i].index > this.model.index && (nextMatch == null || nextMatch.index > this.options[i].index)){
							nextMatch = this.options[i];
						}
					}
				}
			}
			if(nextMatch){
				return nextMatch;
			}else if(firstMatch){
				return firstMatch;
			}
			return null;
		}
		
		this.clone = function(){
			var clone = new SingleSelect(cloneObj(params));
			var options = [];
			var modelIndex = 0;
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i] == this.model){
					modelIndex = i;
				}
				if(!this.options[i].isAggregate){
					options.push(angular.copy(this.options[i]));
				}
			}
			clone.fillOptions(options);
			clone.setModel(clone.options[modelIndex]);
			return clone;
		}
	}
	
	return {
		InstanceClass: SingleSelect
	}
}]);


backendApp.service('Multiselect', [function(){
	var Multiselect = function(params){
		var that = this;
		
		this.visible = false;
		this.isSmart = params.isSmart;
		this.noSort = params.noSort;
		this.sortById = params.sortById;
		this.mutateCallback = params.mutateCallback;
		this.mutateOnlyAggregate = params.mutateOnlyAggregate;
		this.iconCallback = params.iconCallback;
		
		this.filterText = '';
		this.filterCallback = params.filterCallback;
		
		this.options = [];
		this.aggregateOption = params.aggregateOption;
		if(this.aggregateOption){
			this.aggregateOption.isAggregate = true;
			this.aggregateOption.checked = true;
			this.aggregateOption.visible = true;
			this.options.push(this.aggregateOption);
		}
		
		this.fillOptions = function(options){
			this.options = [];
			if(this.aggregateOption){
				this.options.push(this.aggregateOption);
			}
			if(options && Array.isArray(options)){
				for(var i = 0; i < options.length; i++){
					if(!options[i].checked){
						options[i].checked = false;
					}
					if(!options[i].visible){
						options[i].visible = true;
					}
					this.options.push(options[i]);
				}
			}else if(options){
				for(var el in options){
					if(options.hasOwnProperty(el)){
						this.options.push({id: el, name: options[el], checked: false, visible: true});
					}
				}
			}
		}
		
		this.setFilterCallback = function(callback){
			this.filterCallback = callback;
		}
		
		this.filter = function(option){
			var filterTextMatch = true;
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				filterTextMatch = that.mutateCallback(option.name).search(new RegExp(that.filterText, "i")) != -1;
			}else{
				filterTextMatch = option.name.search(new RegExp(that.filterText, "i")) != -1;
			}
			if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
				var filterCallbackResult = that.filterCallback(option);
				if(!filterCallbackResult){
					if(option.checked){
						that.toggle(option);
					}
				}
				return filterTextMatch && filterCallbackResult;
			}
			return filterTextMatch;
		}
		
		this.sort = function(option){
			if(that.noSort){
				return '';
			}
			if(that.sortById){
				return option.isAggregate ? -1000 : parseInt(option.id);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				return option.isAggregate ? '' : that.mutateCallback(option.name).trim();
			}
			return option.isAggregate ? '' : option.name.trim();
		}
		
		this.toggle = function(option){
			if(option.isAggregate){
				if(option.checked){
					return;
				}
				option.checked = !option.checked;
				for(i = 0; i < this.options.length; i++){
					if(this.options[i].isAggregate){
						continue;
					}
					this.options[i].checked = false;
				}
			}else{
				option.checked = !option.checked;
				if(this.aggregateOption){
					this.aggregateOption.checked = false;
				}
				var checkedCount = 0;
				for(i = 0; i < this.options.length; i++){
					if(this.options[i].checked){
						checkedCount++;
					}
				}
				if(checkedCount == 0){
					if(this.aggregateOption){
						this.aggregateOption.checked = true;
					}
				}else if((!this.aggregateOption && checkedCount == this.options.length) || (this.aggregateOption && checkedCount == this.options.length - 1)){
					if(this.aggregateOption){
						this.aggregateOption.checked = true;
						for(i = 0; i < this.options.length; i++){
							if(!this.options[i].isAggregate){
								this.options[i].checked = false;
							}
						}
					}
				}
			}
		}
		
		this.getCheckedIds = function(includeAggregate){
			var arr = [];
			for(var i = 0; i < this.options.length; i++){
				if((includeAggregate || !this.options[i].isAggregate) && this.options[i].checked){
					arr.push(this.options[i].id);
				}
			}
			return arr;
		}
		
		this.setSelectedOptions = function(ids){
			for(var i = 0; i < ids.length; i++){
				var key = -1;
				if((key = searchJsonKeyInArray(this.options, 'id', ids[i])) > -1){
					this.toggle(this.options[key]);
				}
			}
		}
		
		this.getOptionById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return null;
			}
			return val;
		}
		
		this.getElNameById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return '';
			}
			if(this.mutateCallback && (!this.mutateOnlyAggregate || val.isAggregate)){
				return this.mutateCallback(val.name);
			}
			return val.name;
		}
	}
	
	return {
		InstanceClass: Multiselect
	}
}]);

backendApp.service('fileUpload', ['$http','growl','$rootScope', function ($http, growl, $rootScope) {
    this.uploadFileToUrl = function(fileUpload, fileType, uploadUrl){
       var fd = new FormData();
       fd.append('fileUpload', fileUpload);
       fd.append('request', JSON.stringify(fileType));
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
       .success(function(){
    	   $rootScope.$state.go('ln.admin.files');
       })
       .error(function(){
       });
    }
 }]);

backendApp.factory('productsPriorityService', ['$http', 'Utils', function($http, Utils) {
	var priorityProducts = [];
	
	function getPriorityProductsReruest() {
		showLoading();
		return $http.post('product/getPriorityProducts', {})
			.then(function(response) {
				priorityProducts = Utils.parseResponse(response)
					.sort(function(a, b) {
						return b.priority - a.priority;
					});
				hideLoading();
			});
	}
	
	function updateProductsPriorityReruest(data) {
		var ajaxData = {
			data: data
		};
		return $http.post('product/updateProductsPriority', ajaxData);
	}
	
	function getPriorityProducts() {
		return priorityProducts;
	}
	
	function updateProductsPriority() {
		showLoading();
		var priorityData = priorityProducts.map(function(product, index, array) {
			return {
				id: product.id,
				priority: array.length - index
			};
		});
		return updateProductsPriorityReruest(priorityData)
			.then(getPriorityProductsReruest);
	}
	
	function loadNewPriorityProducts() {
		getPriorityProductsReruest();
	}
	
	return {
		getPriorityProducts: getPriorityProducts,
		loadNewPriorityProducts: loadNewPriorityProducts,
		updateProductsPriority: updateProductsPriority
	};
}]);

backendApp.factory('TranslationsService', ['$http', function($http) {
	
	function getMsgResLanguageByKey(data) {
		var methodRequest = {
			data: data
		};
		return $http.post('message/get/msgreslanguage/bykey', methodRequest);
	}
	
	function getMissingLanuages(data) {
		var methodRequest = {
			data: data
		};
		return $http.post('message/get/msgreslanguage/missinglanguages', methodRequest);
	}
	
	function updateMsgResLanguage(msgResLanguageList) {
		var data = msgResLanguageList.map(function(msg) {
			var id = msg.id;
			var actionSourceId = msg.actionSourceId;
			var key = msg.key;
			var msgResLanguageId = msg.msgResLanguageId;
			var value = msg.type == 'html' ? msg.html : msg.value;
			var valueKey = (msg.type == 'html' || value.length > 2000) ? 'largeValue' : 'value';
			
			var result = {
				msgRes: {
					id: id,
					actionSourceId: actionSourceId,
					key: key
				},
				msgResLanguageId: msgResLanguageId
			};
			result[valueKey] = value;
			
			return result;
		});
		
		var methodRequest = {
			data: data
		};
		return $http.post('message/update/msgreslanguage', methodRequest);
	}
	
	function insertMsgResLanguage(dataObj) {
		var key = dataObj.key;
		var actionSourceId = dataObj.actionSourceId;
		var languages = dataObj.languages;
		
		var msgRes = {
			key: key,
			actionSourceId: actionSourceId,
			typeId: 1
		};
		
		var data = languages
			.filter(function(language) {
				var value = dataObj.type == 'html' ? language.html : language.value;
				return value;
			})
			.map(function(language, index) {
				var languageId = language.languageId;
				var value = dataObj.type == 'html' ? language.html : language.value;
				var valueKey = (dataObj.type == 'html' || value.length > 2000) ? 'largeValue' : 'value';
				msgRes.typeId = (dataObj.type == 'html' || value.length > 2000) ? 2 : 1;
				
				var result = {};
				
				if (index === 0) {
					result.msgRes = msgRes;
				}
				
				result.msgResLanguageId = languageId;
				result[valueKey] = value;
				
				return result;
			});

		var methodRequest = {
			data: data
		};
		return $http.post('message/insertMessageResource', methodRequest);
	}
	
	return {
		getMsgResLanguageByKey: getMsgResLanguageByKey,
		insertMsgResLanguage: insertMsgResLanguage,
		updateMsgResLanguage: updateMsgResLanguage,
		getMissingLanuages: getMissingLanuages,
	};
}]);

backendApp.service('FiltersService', ['$http', '$q', function($http, $q) {
	 var cache = {};
	 
	 this.getFilters = function(filtersList) {
	  var data = filtersList.filter(function(f) {
	   return !(f in cache);
	  });
	  
	  if (!data.length) {
	   return $q.resolve(cache);
	  }
	  
	  var methodRequest = {
	   data: data
	  };
	  
	  return $http.post('filters/get', methodRequest).then(function(res) {
	   if (res.data && res.data.responseCode === 0) {
	    var filters = res.data.data;
	    cache = angular.merge({}, cache, filters);
	    return $q.resolve(cache);
	   }
	   return $q.reject(res);
	  }); 
	 };
	}]);

backendApp.provider('lastRequestError', function () {
    var lastError = {
    	errMsg: ''
    };
    
    function setError(errMsg) {
    	lastError = {
    		errMsg: errMsg
    	};
    }
    return {
    	setError: setError,
    	$get: function () {
    		return {
    	    	setError: setError,
    			getError: function() {
    				return lastError;
    			}
    		};
    	}
    };
});
backendApp.factory('userService', ['$http' , '$rootScope', 'growl' , function ($http, $rootScope, growl) {
	var loadedUser = {};
	function getLoadedUser() {		
		return loadedUser;
	}
	
	function logoutUser() {
		loadedUser = {};
	}
	
	function loadUser(id, email) {
		var methodRequest = {
			data: {
				id: id,
				email: email
			}
		};
        $http.post('user/get', methodRequest).then(
        	function (response) {
        		loadedUser = Object(response.data.data) === response.data.data ? response.data.data : {}; 
        		if (loadedUser.id != null) {
        			growl.success($rootScope.getMsgs('user-load-success'));
        		}
			}, function (response) {
				loadedUser = {};
			});
	}
	
	return {
		getLoadedUser: getLoadedUser,
		loadUser: loadUser,
		logoutUser: logoutUser
	}
}]);


backendApp.animation('.slide-animation', function() {
	var defaultDuration = 250;
	function getAnimationDuration($element) {
		return parseInt($element.attr('data-slide-animation-duration')) || defaultDuration;
	}
	return {
		enter: function(element, done) {
			var $element = $(element);
			var duration = getAnimationDuration($element);
			$element
				.css('display', 'none')
				.slideDown(duration, done);
		},
		leave: function(element, done) {
			var $element = $(element);
			var duration = getAnimationDuration($element);
			$element
				.slideUp(duration, done);
		}
	};
});