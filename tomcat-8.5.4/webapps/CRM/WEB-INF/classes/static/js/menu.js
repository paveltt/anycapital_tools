var menu = [{
	permission: '*',
	title: 'home',
	link: 'ln.home'
},{
	title: 'products',
	nodes: [
		{
			permission: 'products_products_view',
			link: 'ln.admin.allProducts',
			title: 'view-all'
		},{
			permission: 'products_products_add',
			link: 'ln.admin.products',
			title: 'add-product'
		},{
			permission: 'products_product-priorty_view',
			link: 'ln.admin.productsPriority',
			title: 'priority'
		}
	]
},{
	title: 'content',
	nodes: [
		{
			permission: 'content_translations_view',
			link: 'ln.admin.translations',
			title: 'translations'
		},{
			permission: 'content_bulk-translations_view',
			link: 'ln.admin.bulkTranslations',
			title: 'bulk-translations'
		}		
	]
},{
	title: 'marketing',
	nodes: [	        
		{
			permission: 'marketing_marketing-sources_view',
			link: 'ln.admin.sources',
			title: 'sources'
		},{
			permission: 'marketing_marketing-lps_view',
			link: 'ln.admin.landingPages',
			title: 'landing-pages'
		},{
			permission: 'marketing_marketing-contents_view',
			link: 'ln.admin.contents',
			title: 'contents'
		},{
			permission: 'marketing_marketing-campaigns_view',
			link: 'ln.admin.campaigns',
			title: 'campaigns'
		},{
			permission: 'marketing_upload-contacts_view',
			link: 'ln.admin.uploadContacts',
			title: 'upload-contacts'
		}
	]
},{
	title: 'administrator',
	nodes: [
		{    
				permission: 'administrator_writer-change-pass_view',
				link: 'ln.admin.writer-change-pass',
				title: 'writer-change-password'			
		},{    
			permission: 'administrator_admin-panel_view',
			link: 'ln.admin.admin-panel',
			title: 'admin-panel'			
		},{    
			permission: 'administrator_writers_view',
			link: 'ln.admin.writers',
			title: 'writers'			
		},{    
	    	permission: 'administrator_db-parameters_view',
	    	link: 'ln.admin.dbParameters',
	    	title: 'db-parameters'			
	    },{
			permission: 'administrator_investments_view',
			link: 'ln.admin.investments',
			title: 'investments'			
		},{
			       
			permission: 'administrator_transactions_view',
			link: 'ln.admin.allTransactions',
			title: 'transactions'			
		},{
			title: 'pending-withdraws',
			nodes: [
			        
				{
					permission: 'administrator_pending-withdraws-fa_view',
					link: 'ln.admin.firstApproval',
					title: 'first-approval'
				},{
					permission: 'administrator_pending-withdraws-sa_view',
					link: 'ln.admin.secondApproval',
					title: 'second-approval'
				}
			]
		}
	]
},{
	title: 'users',
	nodes: [
	        
	    {
			permission: 'users_users_view',
			link: 'ln.admin.loadUser',
			title: 'load-user'
		},{
			permission: 'users_client-space_view',
			link: 'ln.admin.clientSpace',
			title: 'client-space'
		},{
			permission: 'users_user-advanced-search_view',
			link: 'ln.admin.advancedSearchUser',
			title: 'user-advanced-search'
		},{
			permission: 'users_users_add',
			link: 'ln.admin.addUser',
			title: 'add-user'
		},{
			title: 'user-id', 
			userRequired: true,
			nodes: [
				{
					permission: 'users_users_edit',
					link: 'ln.admin.editUser',
					title: 'edit-user'
				},{
					permission: 'users_user-issues_view',
					link: 'ln.admin.issues',
					title: 'issues'
				},{
					permission: 'users_user-questionnaire_view',
					link: 'ln.admin.userAnswers',
					title: 'user-answers'
				},{
					permission: 'users_files_view',
					link: 'ln.admin.files',
					title: 'files'
				},{
					permission: 'users_regulation_view',
					link: 'ln.admin.regulation',
					title: 'regulation'
				},{
					permission: 'users_user-change-pass_view',
					link: 'ln.admin.changePassword',
					title: 'change-password'
				},{
					permission: 'users_user-transactions_view',
					link: 'ln.admin.transactions',
					title: 'transactions'
				},{
					permission: 'users_user-investments_view',
					link: 'ln.admin.userInvestments',
					title: 'investments'
				},{
					permission: 'users_templates_view',
					link: 'ln.admin.templates',
					title: 'templates'
				},{
			    	title: 'deposit',
			    	nodes: [
			    	        {
			    	        	permission: 'users_user-wire-deposit_view',
						    	link: 'ln.admin.wireDeposit',
						    	title: 'wire',
			    	        },{
			    	        	permission: 'users_user-admin-deposit_view',
						    	link: 'ln.admin.adminDeposit',
						    	title: 'administrator',
			    	        }
			    	   ]
			    },{
			    	title: 'withdraw',
			    	nodes: [
			    	        {
			    	        	permission: 'users_user-wire-withdraw_view',
						    	link: 'ln.admin.wireWithdraw',
						    	title: 'wire',
			    	        },{
			    	        	permission: 'users_user-admin-withdraw_view',
						    	link: 'ln.admin.adminWithdraw',
						    	title: 'administrator',
			    	        }
			    	   ]
			    }
			]
		}
	]
}];


