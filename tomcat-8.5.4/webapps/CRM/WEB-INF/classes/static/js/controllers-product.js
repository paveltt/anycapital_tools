backendApp.controller('ProductsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect', 'Multiselect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Multiselect, growl) {
	$scope.action = 'loading';
	$scope.init = {};
	$scope.filter = {};
	$scope.settings = Utils.settings;
	$scope.dateManager = new Utils.DateManager();
	$scope.productTypeFilter = function(option){
		if($scope.action == 'edit'){
			return true;
		}
		if($scope.init.product_types[option.id].productCategory.id == $scope.filter.productCategory.getId()){
			return true;
		}
		return false;
	}
	$scope.selectedProduct = {};
	$scope.initFiltersOnReady = function(){
		$scope.filter.productCategory = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});
		$scope.filter.productType = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct.productType;}, key: 'id'}, filterCallback: $scope.productTypeFilter});
		$scope.filter.dayCountConvention = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct.dayCountConvention;}, key: 'id'}});
		$scope.filter.productInsuranceType = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct;}, key: 'productInsuranceTypeId'}});
		$scope.filter.currency = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct;}, key: 'currencyId'}});
		$scope.filter.productCouponFrequency = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct.productCouponFrequency;}, key: 'id'}});
		$scope.filter.markets = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});
		$scope.filter.dayCountConvention = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct.dayCountConvention;}, key: 'id'}});
		$scope.filter.productBarrierType = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct.productBarrier? $scope.selectedProduct.productBarrier.productBarrierType : null;}, key: 'id'}});
		$scope.filter.productMaturity = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs, externalModel: {objCallback: function(){return $scope.selectedProduct.productMaturity;}, key: 'id'}});
	}
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getInsertProductInit: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getInsertProductInit('initList');});
	$scope.checkAction = function(){
		if($rootScope.$stateParams.productId){
			$scope.action = 'edit';
			$scope.productId = $rootScope.$stateParams.productId;
			$scope.getProductFull();
		}else if($rootScope.$state.includes("ln.admin.products.add")){
			$scope.action = 'add';
			$scope.filter.productCategory.setModel(null);
			$scope.selectedProduct = {};
			$scope.selectedProduct.id = 0;
			$scope.selectedProduct.productType = {id: 0};
			$scope.selectedProduct.denomination = 1000;
			$scope.selectedProduct.currencyId = 3;//EUR
			$scope.selectedProduct.issuePrice = 100;
			$scope.selectedProduct.productCouponFrequency = {};
			$scope.selectedProduct.productScenarios = [];
			$scope.selectedProduct.productCoupons = [];
			$scope.selectedProduct.productAutocalls = [];
			$scope.selectedProduct.productCallables = [];
			$scope.selectedProduct.productSimulation = [];
			$scope.selectedProduct.dayCountConvention ={};
			$scope.selectedProduct.productBarrierType = {};
			$scope.selectedProduct.productMaturity = {};
						
		}else{
			$scope.action = '';
		}
	}
	
	$scope.getInsertProductInit = function(dependencyName){
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var productCategories = "product_categories";
		var productTypes = "product_types";
		var dayCountConvention = "day_count_convention";
		var markets = "markets";
		var currencies = "currencies";
		var productCouponFrequencies = "product_coupon_frequencies";
		var productBarrierType = "product_barrier_type";
		var productMaturity = "product_maturity"
		var productIssuerInsuranceType = "product_issuer_insurance_type"
		var productStatuses = "product_statuses";	
		var methodRequest = {};
		 methodRequest.data = [];
		    methodRequest.data.push (
		    		productCategories,
		    		productTypes,
		    		dayCountConvention,
		    		markets,
		    		currencies,
		    		productCouponFrequencies,
		    		productBarrierType,
		    		productMaturity,
		    		productIssuerInsuranceType
		    ) 
			$http.post('filters/get', methodRequest).then(
				function (response) {
					$scope.hideLoading();
					$rootScope.markDependencyDone($scope, dependencyName, 'getInsertProductInit');
					if (!$scope.handleErrors(response, 'getInsertProductInit')) {
						$scope.init = Utils.parseResponse(response);
						$scope.filter.productCategory.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_categories));
						$scope.filter.productType.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_types));
						$scope.filter.dayCountConvention.fillOptions($scope.parseFilter(Utils.parseResponse(response).day_count_convention));
						$scope.filter.productInsuranceType.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_issuer_insurance_type));
						$scope.filter.markets.fillOptions($scope.parseFilter(Utils.parseResponse(response).markets));
						$scope.filter.currency.fillOptions($scope.parseFilter(Utils.parseResponse(response).currencies));
						$scope.filter.productCouponFrequency.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_coupon_frequencies));
						$scope.filter.productBarrierType.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_barrier_type));
						$scope.filter.productMaturity.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_maturity));
					}
				}, function (response) {
					$scope.hideLoading();
					$scope.handleNetworkError(response);
				});		  	
	}
	$scope.parseFilter = function(filter){
		var arr = [];
		for(key in filter){
			if(filter.hasOwnProperty(key)){
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}

	$scope.add = function(){
		$rootScope.$state.go('ln.admin.products.add', {ln: $rootScope.$state.params.ln});
	}
	
	$scope.isFieldVisible = function(field){
		if($rootScope.isEmpty($scope.selectedProduct)){
			return true;
		}
		var productTypeId = 0;
		if($scope.action == 'add'){
			productTypeId = $scope.filter.productType.getId();
		}else{
			productTypeId = $scope.selectedProduct.productType.id;
		}
		var productType = $scope.init.product_types[productTypeId];
		
		if(productType){
			var productCategoryId = productType.productCategory.id;
			var productCategory = $scope.init.product_categories[productCategoryId];
			
			if(productCategory[field] === false || productType[field] === false){
				return false;
			}
		}
		return true;
	}
	
	
	$scope.getProductFull = function() {
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId
		};
		$http.post('product/getProductFull', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'getProductFull')) {
					$scope.selectedProduct = Utils.parseResponse(response);
					
					//Parse amounts
					$scope.selectedProduct.denomination = $rootScope.formatAmount({amount: $scope.selectedProduct.denomination, amountOnly: true});
					$scope.selectedProduct.issueSize = $rootScope.formatAmount({amount: $scope.selectedProduct.issueSize, amountOnly: true});
					
					//Parse dates
					if ($scope.selectedProduct.productBarrier) {
						$scope.selectedProduct.productBarrier.barrierStart = new Date($scope.selectedProduct.productBarrier.barrierStart - (new Date($scope.selectedProduct.productBarrier.barrierStart)).getTimezoneOffset() * 60 * 1000);
						$scope.selectedProduct.productBarrier.barrierEnd = new Date($scope.selectedProduct.productBarrier.barrierEnd - (new Date($scope.selectedProduct.productBarrier.barrierEnd)).getTimezoneOffset() * 60 * 1000);
					}
					$scope.selectedProduct.subscriptionStartDate = new Date($scope.selectedProduct.subscriptionStartDate - (new Date($scope.selectedProduct.subscriptionStartDate)).getTimezoneOffset() * 60 * 1000);
					//var subscriptionEndDate = new Date($scope.selectedProduct.subscriptionEndDate - (new Date($scope.selectedProduct.subscriptionEndDate)).getTimezoneOffset() * 60 * 1000);
					if ($scope.action == 'add') {
						$scope.selectedProduct.subscriptionEndDate = new Date($scope.selectedProduct.subscriptionEndDate - (new Date($scope.selectedProduct.subscriptionEndDate)).getTimezoneOffset() * 60 * 1000);
					} else {
						$scope.selectedProduct.subscriptionEndDate = new Date($scope.selectedProduct.subscriptionEndDate);
					}
					$scope.selectedProduct.initialFixingDate = new Date($scope.selectedProduct.initialFixingDate - (new Date($scope.selectedProduct.initialFixingDate)).getTimezoneOffset() * 60 * 1000);
					$scope.selectedProduct.issueDate = new Date($scope.selectedProduct.issueDate - (new Date($scope.selectedProduct.issueDate)).getTimezoneOffset() * 60 * 1000);
					$scope.selectedProduct.firstExchangeTradingDate = new Date($scope.selectedProduct.firstExchangeTradingDate - (new Date($scope.selectedProduct.firstExchangeTradingDate)).getTimezoneOffset() * 60 * 1000);
					$scope.selectedProduct.lastTradingDate = new Date($scope.selectedProduct.lastTradingDate - (new Date($scope.selectedProduct.lastTradingDate)).getTimezoneOffset() * 60 * 1000);
					$scope.selectedProduct.finalFixingDate = new Date($scope.selectedProduct.finalFixingDate - (new Date($scope.selectedProduct.finalFixingDate)).getTimezoneOffset() * 60 * 1000);
					$scope.selectedProduct.redemptionDate = new Date($scope.selectedProduct.redemptionDate - (new Date($scope.selectedProduct.redemptionDate)).getTimezoneOffset() * 60 * 1000);
					
					for(var i = 0; i < $scope.selectedProduct.productMarkets.length; i++){
						//$scope.filter.markets.setSelectedOptions([$scope.selectedProduct.productMarkets[i].market.id]);
						$scope.filter.markets.setModel($scope.filter.markets.getOptionById($scope.selectedProduct.productMarkets[i].market.id));
						break;
					}
					
					for(var i = 0; i < $scope.selectedProduct.productCoupons.length; i++){
						$scope.selectedProduct.productCoupons[i].observationDate = new Date($scope.selectedProduct.productCoupons[i].observationDate);
						$scope.selectedProduct.productCoupons[i].paymentDate = new Date($scope.selectedProduct.productCoupons[i].paymentDate);
					}
					for(var i = 0; i < $scope.selectedProduct.productAutocalls.length; i++){
						$scope.selectedProduct.productAutocalls[i].examinationDate = new Date($scope.selectedProduct.productAutocalls[i].examinationDate);
					}
					for(var i = 0; i < $scope.selectedProduct.productCallables.length; i++){
						$scope.selectedProduct.productCallables[i].examinationDate = new Date($scope.selectedProduct.productCallables[i].examinationDate);
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.removeItemFromArr = function(arr, index){
		arr.splice(index, 1);
	}
	
	$scope.addScenario = function(){
		if(!$scope.selectedProduct.productScenarios){
			$scope.selectedProduct.productScenarios = [];
		}
		$scope.selectedProduct.productScenarios.push({
			position: '',
			finalFixingLevel: '',
			text: ''
		});
	}
	$scope.addCoupon = function(){
		if(!$scope.selectedProduct.productCoupons){
			$scope.selectedProduct.productCoupons = [];
		}
		$scope.selectedProduct.productCoupons.push({
			observationDate: '',
			paymentDate: '',
			triggerLevel: '',
			payRate: ''
		});
	}
	$scope.addAutocall = function(){
		if(!$scope.selectedProduct.productAutocalls){
			$scope.selectedProduct.productAutocalls = [];
		}
		$scope.selectedProduct.productAutocalls.push({
			examinationDate: '',
			condition: ''
		});
	}
	$scope.addCallable = function(){
		if(!$scope.selectedProduct.productCallables){
			$scope.selectedProduct.productCallables = [];
		}
		$scope.selectedProduct.productCallables.push({
			examinationDate: ''
		});
	}
	$scope.addSimulation = function(){
		if(!$scope.selectedProduct.productSimulation){
			$scope.selectedProduct.productSimulation = [];
		}
		$scope.selectedProduct.productSimulation.push({
			position: '',
			finalFixingLevel: '',
			cashSettlement: ''
		});
	}
	
	
	$scope.generateInsertRequest = function(){
		var methodRequest = {};
		
		if($rootScope.isEmpty($scope.selectedProduct)){
			return methodRequest;
		}
		
		methodRequest.data = cloneObj($scope.selectedProduct);
		if($scope.action == 'add'){
			methodRequest.data.productType = {};
			methodRequest.data.productType.id = $scope.filter.productType.getId();
		}
		
		methodRequest.data.dayCountConvention = {};
		methodRequest.data.dayCountConvention.id = $scope.filter.dayCountConvention.getId();
		
		methodRequest.data.productMaturity = {};
		methodRequest.data.productMaturity.id = $scope.filter.productMaturity.getId();
				
		//Parse amounts
		methodRequest.data.denomination = $rootScope.formatAmountForDb({amount: methodRequest.data.denomination});
		methodRequest.data.issueSize = $rootScope.formatAmountForDb({amount: methodRequest.data.issueSize});
		
		//Parse dates
		if(methodRequest.data.productBarrier){
			methodRequest.data.productBarrier.productBarrierType = {};
			methodRequest.data.productBarrier.productBarrierType.id = $scope.filter.productBarrierType.getId();
			methodRequest.data.productBarrier.barrierStart = methodRequest.data.productBarrier.barrierStart ? methodRequest.data.productBarrier.barrierStart.getTime() : '';
			methodRequest.data.productBarrier.barrierEnd = methodRequest.data.productBarrier.barrierEnd ? methodRequest.data.productBarrier.barrierEnd.getTime() : '';
		}
		methodRequest.data.subscriptionStartDate = methodRequest.data.subscriptionStartDate ? methodRequest.data.subscriptionStartDate.getTime() : '';
		methodRequest.data.subscriptionEndDate = methodRequest.data.subscriptionEndDate ? methodRequest.data.subscriptionEndDate.getTime() : '';
		methodRequest.data.initialFixingDate = methodRequest.data.initialFixingDate ? methodRequest.data.initialFixingDate.getTime() : '';
		methodRequest.data.issueDate = methodRequest.data.issueDate ? methodRequest.data.issueDate.getTime() : '';
		methodRequest.data.firstExchangeTradingDate = methodRequest.data.firstExchangeTradingDate ? methodRequest.data.firstExchangeTradingDate.getTime() : '';
		methodRequest.data.lastTradingDate = methodRequest.data.lastTradingDate ? methodRequest.data.lastTradingDate.getTime() : '';
		methodRequest.data.finalFixingDate = methodRequest.data.finalFixingDate ? methodRequest.data.finalFixingDate.getTime() : '';
		methodRequest.data.redemptionDate = methodRequest.data.redemptionDate ? methodRequest.data.redemptionDate.getTime() : '';
		
		methodRequest.data.productMarkets = [];
		/*var checkedIds = $scope.filter.markets.getCheckedIds();
		for(var i = 0; i < checkedIds.length; i++){
			methodRequest.data.productMarkets.push({
				market: {
					id: checkedIds[i]
				},
				marketId: checkedIds[i]
			});
		}*/
		methodRequest.data.productMarkets.push({
			market: {
				id: $scope.filter.markets.getId()
			},
			id: $scope.selectedProduct.productMarkets && $scope.selectedProduct.productMarkets[0] ? $scope.selectedProduct.productMarkets[0].id : 0
		});
		
		if(methodRequest.data.productScenarios){
			for(var i = 0; i < methodRequest.data.productScenarios.length; i++){
				methodRequest.data.productScenarios[i].position = i + 1;
			}
		}
		
		if(methodRequest.data.productCoupons){
			for(var i = 0; i < methodRequest.data.productCoupons.length; i++){
				methodRequest.data.productCoupons[i].observationDate = $rootScope.dateParse({date: methodRequest.data.productCoupons[i].observationDate, startOfDay: true, timestamp: true});
				methodRequest.data.productCoupons[i].paymentDate = $rootScope.dateParse({date: methodRequest.data.productCoupons[i].paymentDate, startOfDay: true, timestamp: true});
			}
		}
		if(methodRequest.data.productAutocalls){
			for(var i = 0; i < methodRequest.data.productAutocalls.length; i++){
				methodRequest.data.productAutocalls[i].examinationDate = $rootScope.dateParse({date: methodRequest.data.productAutocalls[i].examinationDate, startOfDay: true, timestamp: true});
			}
		}
		if(methodRequest.data.productCallables){
			for(var i = 0; i < methodRequest.data.productCallables.length; i++){
				methodRequest.data.productCallables[i].examinationDate = $rootScope.dateParse({date: methodRequest.data.productCallables[i].examinationDate, startOfDay: true, timestamp: true});
			}
		}
		
		if(methodRequest.data.productSimulation){
			for(var i = 0; i < methodRequest.data.productSimulation.length; i++){
				methodRequest.data.productSimulation[i].position = i + 1;
			}
		}
		
		return methodRequest;
	}
	
	$scope.insertProductFull = function() {
		$scope.showLoading();
		var methodRequest = $scope.generateInsertRequest();
		$http.post('product/insertProductFull', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (response.data.responseCode == 0) {
					$rootScope.$state.go('ln.admin.allProducts', {ln: $rootScope.$state.params.ln});
					if($scope.action == 'edit'){
						growl.success("product edited successfully");
					}
					else if($scope.action == 'add'){
						growl.success("product added successfully");
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	
}]);
backendApp.controller('AllProductsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'Pagination', 'SingleSelect', '$location', '$window' ,'growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, Pagination, SingleSelect, $location, $window, growl) {
	$scope.myUrl = $location.absUrl();
	$scope.userOffset = new Date().getTimezoneOffset();
	$scope.init = {};
	$scope.filter = {};
	$scope.dateManager = new Utils.DateManager();
	$scope.filter.subscriptionDate;
	$scope.productsArr = [];
	$scope.filtered = {};
	$scope.filtered.productsArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.productsArr.length, $scope.resultsPerPage);
		});
	}
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.filtered.productsArr.length, $scope.resultsPerPage);
	}
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});	
	$scope.initFiltersOnReady = function () {
		$scope.filter.productCategory = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
		$scope.filter.productType = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
		$scope.filter.market = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
		$scope.filter.productStatus = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
	}
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getProductsInit: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getProductsInit('initList');});
	$scope.getProductsInit = function (dependencyName) {
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var productStatuses = "product_statuses";
		var productTypes = "product_types";
		var productCategories = "product_categories";
		var markets = "markets";
		var methodRequest = {};
		methodRequest.data = 0;
	    methodRequest.data = [];
	    methodRequest.data.push (
	    		productStatuses,
	    		productTypes,
	    		productCategories,
	    		markets
	    ) 
		$http.post('filters/get', methodRequest).then(
			function (response) {
				$scope.hideLoading();
				$rootScope.markDependencyDone($scope, dependencyName, 'getProductsInit');
				if (!$scope.handleErrors(response, 'getProductsInit')) {
					$scope.init = Utils.parseResponse(response);
					$scope.filter.productCategory.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_categories));
					$scope.filter.productType.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_types));
					$scope.filter.market.fillOptions($scope.parseFilter(Utils.parseResponse(response).markets));
					$scope.filter.productStatus.fillOptions($scope.parseFilter(Utils.parseResponse(response).product_statuses));
				}
			}, function (response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});		
	}
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}
	$scope.listProductsFilter = function (product) {
		var hide = false;
		// product category filter
	    if ($scope.filter.productCategory.getId() > -1) {
				if ($scope.filter.productCategory.getId() != product.categoryId) {
					hide = true;
				}
		}
		// product type filter
	    if ($scope.filter.productType.getId() > -1) {
				if ($scope.filter.productType.getId() != product.typeId) {
					hide = true;
				}
		}
		// product status filter
		if ($scope.filter.productStatus.getId() > -1) {
				if($scope.filter.productStatus.getId() != product.status) {
					hide = true;
				}
		}
		// product market filter
		if ($scope.filter.market.getId() > -1) {				
				if ($scope.filter.market.getId() != product.marketId) {
					hide = true;
				}
		}
		// product subscription date filter        
//		if ($scope.filter.subscriptionDate != null) {
//			if( $scope.filter.subscriptionDate.getTime() != product.subscription) {
//				hide = true;
//			}
//		}
		return !hide;
	}
	$scope.search = function () {
		$scope.isLoading = true;
		var methodRequest = {};
		methodRequest.data = {
			subscriptionStartDate : $scope.filter.subscriptionDate ? $scope.filter.subscriptionDate.getTime() : null
		};
		$http.post('product/getAllProducts', methodRequest).then(
			function (response) {
				$scope.isLoading = false;
				var products = response.data.data;
				var productsArr = [];
				for (product in products) {
					if (products.hasOwnProperty(product)) {
						var subscription = products[product].subscriptionStartDate;
						subscription = new Date(subscription);
						subscription.setHours(0,0,0,0);
						subscription = subscription.getTime();
						if (products[product].productCoupons[0] != null) {
							var observationDate = products[product].productCoupons[0].observationDate;
							observationDate = new Date(observationDate);
							observationDate = observationDate.getTime();
							var now = new Date();
							now = now.getTime();
						}
						var endTradeLevel = products[product].productMarkets[0].endTradeLevel;
						if (endTradeLevel == null) {
							endTradeLevel = "empty";
						}
						productsArr.push({
							id: products[product].id,
							subscriptionStart: products[product].subscriptionStartDate,
							type: products[product].productType.displayName, 
							typeId: products[product].productType.id,
							category: products[product].productType.productCategory.displayName, 
							categoryId: products[product].productType.productCategory.id,
							market: products[product].productMarkets[0].market.displayName, 
							marketId: products[product].productMarkets[0].market.id,
							subscription: subscription,
							timeCreated :products[product].timeCreated, 
							ask: products[product].ask, 
							bid: products[product].bid, 
							marketId: products[product].productMarkets[0].market.id, 
							startTradeLevel: products[product].productMarkets[0].startTradeLevel, 
							endTradeLevel: endTradeLevel,
							obsDate: products[product].productCoupons[0] == null ? null:products[product].productCoupons[0].observationDate,
							observationDate: observationDate,
							now: now,
							obsLevel: products[product].productCoupons[0] == null ? null:products[product].productCoupons[0].observationLevel,
							prodCouponId: products[product].productCoupons[0] == null ? null:products[product].productCoupons[0].id,
							prodCoupon: products[product].productCoupons[0] == null ? null:products[product].productCoupons[0],
							suspend: products[product].suspend, 
							barrier: products[product].productBarrier == null ? null:products[product].productBarrier.barrierOccur,
							status: products[product].productStatus.id,
							statusName: products[product].productStatus.displayName});
						}
				}
				$scope.productsArr = productsArr;
				$scope.updatePages();
			}, function (response) {
				// failure callback
				$scope.isLoading = false;
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.uploadKID = function(productId) {	
		$scope.productId = productId;
	  var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/upload-KID.html',
			scope: $scope,
			controller: 'uploadKIDPopupCtr',
			windowClass: 'upload-KID-popup'
		});
	}
	
	$scope.askBid = function(productId,ask,bid) {	
		$scope.productId = productId;
		$scope.ask=ask;
		$scope.bid=bid;
	  var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/ask-bid-popup.html',
			scope: $scope,
			controller: 'askBidPopupCtr',
			windowClass: 'ask-bid-popup'
		});
	}
	$scope.startLevel = function (productId, marketId, startTradeLevel) {
		$scope.productId = productId;
		$scope.marketId = marketId;
		$scope.startTradeLevel = startTradeLevel;
		var previewModal = $uibModal.open({ 
			templateUrl: folderPrefix + 'components/start-trade-level-popup.html',
			scope: $scope,
			controller: 'startTradeLevelPopupCtr',
			windowClass: 'start-trade-level-popup'
		});
	}
	$scope.endLevel = function (productId, marketId, endTradeLevel) {
		$scope.productId = productId;
		$scope.marketId=marketId;
		if(endTradeLevel == 'empty'){
			endTradeLevel = '';
		}
		$scope.endTradeLevel=endTradeLevel;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/end-trade-level-popup.html',
			scope: $scope,
			controller: 'endTradeLevelPopupCtr',
			windowClass: 'end-trade-level-popup'
		});
	}
	$scope.updateSuspend = function (productId, suspend) {
		$scope.productId = productId;
		$scope.suspend = suspend;
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.productId,
			suspend: $scope.suspend
		};
		$http.post('product/updateSuspend', methodRequest).then(
			function(response) {
				$scope.search();
				if(response.data.responseCode == 0){
					growl.success('suspend of product '+productId+ ' updated');
				}
			},
			
	        function(response) {
	
			}
		)
	}		
	$scope.updateProductBarrierOccur = function (productId, barrier){
	$scope.productId = productId;
	$scope.barrier = barrier;
	var methodRequest = {};
	methodRequest.data = { barrierOccur: $scope.barrier, productId: $scope.productId};
    $http.post('product/updateProductBarrierOccur', methodRequest).then(
		function(response) {
			$scope.search();
			if(response.data.responseCode == 0){
				growl.success('barrier occur of product '+productId+ ' has been updated');
			}
		},
		
        function(response) {
			
		})
    }	
	$scope.updateStatus = function(productId) {
		$scope.productId = productId;
	    var methodRequest = {};
	    methodRequest.data = {
				id: $scope.productId,
			};
	    $http.post('product/publish', methodRequest).then(
			function(response) {
			    $scope.search();
			    if(response.data.responseCode == 0){
					growl.success('product '+productId+ ' has been published');
				}
	        },
			function(response) {
				
	
			}
	    )
	}
	$scope.routeTo = function ( productId ) {
		var methodRequest = {};
	    methodRequest.data = {};
	    $http.post('app/domain-url', methodRequest).then(
				function(response) {
					$window.open(response.data.data + '#/en/products/' + productId, '_blank');
		        },
		        function(response) {		

				}
	        )
		};
	$scope.obsLevel = function (productCoupon,productId) {
		$scope.productCoupon = productCoupon;
		$scope.productId = productId;
	    var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/update-observation-level-popup.html',
			scope: $scope,
			controller: 'updateObservationLevelPopupCtr',
			windowClass: 'updateObservationLevelPopup'
		});
	}	
}]);
backendApp.controller('updateObservationLevelPopupCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {	
	
	$scope.updateObsLevel = function(prodCouponId,productId,obsLevel) {
		$scope.prodCouponId=prodCouponId;
		$scope.productId = productId;
		$scope.obsLevel=obsLevel;
		 var methodRequest = {};
		    methodRequest.data = {
						id: $scope.prodCouponId,
						productId: $scope.productId,
						observationLevel: $scope.obsLevel
			};
		    $http.post('product/productcoupon/update/observationlevel', methodRequest).then(
					function(response) {
					$scope.search();
					if(response.data.responseCode == 0){
						growl.success('observation level of product '+productId+ ' has been updated');
					}
			        },
					
			        function(response) {		

					}
		        )		
	}
}]);
backendApp.controller('uploadKIDPopupCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {
	$scope.filter = {};
	$scope.kidArr = [];
	$scope.filter.language = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});	
	$scope.myFile = {};
	var languages = "languages";
	var methodRequest = {};
	methodRequest.data = 0;
	methodRequest.data = [];
    methodRequest.data.push(
    		languages
    )
	$http.post('filters/get', methodRequest).then(
		function (response) {
			$scope.isLoading = false;
			$scope.init = Utils.parseResponse(response);
			$scope.filter.language.fillOptions($scope.parseFilter(Utils.parseResponse(response).languages));
			getProductKidLanguages();
		}, function (response) {
			$scope.isLoading = false;
			$scope.handleNetworkError(response);
		});		
	
	$scope.parseFilter = function (filter) {	
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}
	
	function getProductKidLanguages() {
		methodRequest.data = {
				id: $scope.productId
		};
		$http.post('product/getProductKidLanguages', methodRequest).then(
			function (response) {
				$scope.isLoading = false;
				var products = response.data.data;				
				for (product in products) {
					$scope.kidArr.push({
						url: 'https://' + products[product].url,
						language:  products[product].language.displayName 
					});
				}	
			}, function (response) {
				$scope.isLoading = false;
				$scope.handleNetworkError(response);
			});	
	}
	
	$scope.updateKID = function() { 	
		var file = $scope.myFile.file;
	    var request = {};    
	    request = {"actionSource":2}
	    request.data = {};
	    request.data.language = {
	    	id: $scope.filter.language.getId()
	    }
	    request.data.product = {
	        	id: $scope.productId
	    }
	    var uploadUrl = "product/updateKid"; 
	    var fd = new FormData();
	    fd.append('fileUpload', file);
	    fd.append('request', JSON.stringify(request));
	       $http.post(uploadUrl, fd, {
	          transformRequest: angular.identity,
	          headers: {'Content-Type': undefined}
	       })
	       .success(function(){
	    	   growl.success('Update KID successfully');
	       })
	       .error(function(){
	    	   growl.success('error');
	       });
	}
       
}]);
backendApp.controller('askBidPopupCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {	
	$scope.updateAskBid = function() {
    var methodRequest = {};
    methodRequest.data = {
				id: $scope.productId,
				ask: $scope.ask,
				bid: $scope.bid
	};
    $http.post('product/updateAskBid', methodRequest).then(
			function(response) {
				$scope.search();
				if(response.data.responseCode == 0){
					growl.success('ask/bid of product '+ $scope.productId + ' has been updated');
				}
	        },
	        function(response) {		

			}
        )
	}
}]);
backendApp.controller('startTradeLevelPopupCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {
	$scope.updateStartTradeLevel = function() {
		var methodRequest = {};
		methodRequest.data=[];
		methodRequest.data.push({
			productId: $scope.productId,
			market: {
				id: $scope.marketId
			},
			startTradeLevel: $scope.startTradeLevel
		});
		$http.post('product/updateStartTradeLevel', methodRequest).then(
			function(response) {				
				$scope.search();
				if(response.data.responseCode == 0){
					growl.success('start trade level of product '+ $scope.productId + ' has been updated');
				}
	        },
			function(response) {
				
			}
        )
	}
}]);
backendApp.controller('endTradeLevelPopupCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {
	$scope.submitForm = function(isValid) {};
	$scope.updateEndTradeLevel = function() {
		var methodRequest = {};
		methodRequest.data=[];
		methodRequest.data.push({
			productId: $scope.productId,
			market: {
				id: $scope.marketId
			},
			endTradeLevel: $scope.endTradeLevel
		});
		$http.post('product/updateEndTradeLevel', methodRequest).then(
			function(response) {
				$scope.search();
				if(response.data.responseCode == 0){
					growl.success('end trade level of product '+ $scope.productId + ' has been updated');
				}
	        },
	        function(response) {	
			}
        )
	}

}]);